<?php

return [

    /*
        | ---------------------------------------------------------------------
        | Application API Keys
        | ---------------------------------------------------------------------
        |
        | These are the API Keys used by the application to access the bot API
        |
        */

    'ibm' => [
        'iam' => env('IAM_API_KEY', ''),
        'assistant_id' => env('IBM_ASSISTANT_ID', ''),
        'uri' => env('IBM_SERVICE_ENDPOINT', 'https://gateway-lon.watsonplatform.net/assistant/api/'),
    ],

    'telegram' => [
        'key' => env('TELEGRAM_API_KEY', ''),
        'uri' => env('TELEGRAM_SERVICE_ENDPOINT', 'https://api.telegram.org/bot'),
    ],
];