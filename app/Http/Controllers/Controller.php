<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     *  IBM Assistant ID
     */
    protected $assistant_id;
    
     /**
     * Telegram HTTP API Key
     */
    protected $IAMkey;
    
    /**
     * Variale for IBM request client
     */
    protected $IBMClient;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        // IBM Watson Assistant ID
        $this->assistant_id = config('services.ibm.assistant_id');
        // IAM API Key.
        $this->IAMkey = config('services.ibm.iam');
        // Guzzle Request Client.
        $this->IBMClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('services.ibm.uri'),
            // Auth defaults.
            // 'auth' => "apikey:{$this->IAMKey}",
        ]);
    }

    /**
     * Create an IBM Watson Assistant Ssssion
     * 
     * @return string  $id  <Session ID for the created session>
     */
    public function createSession()
    {
        try {
            // Send the request to IBM.
            $res = (array) json_decode(
                $this->IBMClient->request('POST', "v2/assistants/{$this->assistant_id}/sessions?version=2018-11-08")
                    ->getBody()
            );
            $payload['data'] = $res;
        } catch (\Throwable $th) {
            $payload['data'] = $th->getMessage();
        } finally {
            return $payload['data'];
        }
    }

    /**
     * Delete an IBM Watson Assistant Ssssion
     * 
     * @param string  $id  <The Session ID to destroy>
     */
    public function deleteSession($id)
    {
        try {
            // Send the request to IBM.
            $res = (array) json_decode(
                $this->IBMClient->request('DELETE', "v2/assistants/{$this->assistant_id}/sessions/{$id}?version=2018-11-08")
                    ->getBody()
            );
            $payload['data'] = $res;
        } catch (\Throwable $th) {
            $payload['data'] = $th->getMessage();
        } finally {
            return $payload['data'];
        }
    }

    /**
     * Send a message to IBM Assistant.
     * 
     * @param string  $id  <Session ID for the message to send>
     * @param array  $data <The JSON data to send>
     */
    public function sendMessage($id, array $data)
    {
        try {
            // Send the request to IBM.
            $res = (array) json_decode(
                $this->IBMClient->request('POST', "v2/assistants/{$this->assistant_id}/sessions/{$id}/message?version=2018-11-08", [
                    'json' => $data
                ])->getBody()
            );
            $payload['data'] = $res;
        } catch (\Throwable $th) {
            $payload['data'] = $th->getMessage();
        } finally {
            return $payload['data'];
        }
    }
}
