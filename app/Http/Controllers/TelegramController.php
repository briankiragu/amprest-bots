<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TelegramController extends Controller
{
    /**
     * Telegram HTTP API Key
     */
    protected $key;

    /**
     * Variale for Guzzle Client
     */
    protected $telegramClient;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        // Telegram HTTP API Key.
        $this->key = config('services.telegram.key');
        // Guzzle Request Client.
        $this->telegramClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('services.telegram.uri'). $this->key .'/'
        ]);
    }

    /**
     * Process a new user message
     */
    public function newMessage(Request $request, $token)
    {
        try {
            // Check if the URL token matches the Telegram API Key.
            if ($token !== $this->key) {
                $payload['data'] = 'URL Token not authorized';
                $payload['status'] = 401;
            } else {
                // Get the request message object.
                $message = $request->all()['message'];
                $chat_id = $message['chat']['id'];
                $message_text = $message['text'];

                // An array of possible greetings.
                $greetings = [
                    'hey', 'hello', 'hi',
                    'greetings', 'how are you',
                    'good morning', 'good afternoon', 'good evening'
                ];

                // Perform chat logic.
                switch ($message_text) {
                    // Check for greetings.
                    case str_contains(strtolower($message_text), $greetings):
                        $reply = "Hello @{$message['from']['username']}. How can I help you?";
                        break;

                    // Default reply.
                    default:
                        // Create an IBM session.
                        $session_id = $this->createSession();
                        // Create the message element.
                        $messageData = [
                            "input" => [
                                "message_type" => "text",
                                "text" => "Hello. Got a second for a question?",
                                "options" => [
                                    "alternate_intents" => true,
                                    "debug" => true
                                ]
                            ],
                            "context" => [
                                "global" => [
                                    "system" => [
                                        "user_id" => $message['from']['id']
                                    ]
                                ]
                            ]
                        ];
                        // Call the IBM sendMessage method.
                        $res = $this->sendMessage($session_id, $messageData);
                        $reply = $res['output']['generic']['text'];
                        // Destroy the IBM session.
                        $this->deleteSession($session_id);
                        break;
                }

                // Prepare the request data.
                $data = [
                    'chat_id' => $chat_id,
                    'text' => $reply
                ];

                // Send the request to Telegram.
                $res = (array) json_decode(
                    $this->telegramClient->request('POST', 'sendMessage?'. http_build_query($data))
                        ->getBody()
                );
                $payload['data'] = $res;
                $payload['status'] = 200;
            }
        } catch (\Throwable $th) {
            $payload['data'] = $th->getMessage();
            $payload['status'] = 401;
        } finally {
            return response($payload['data'], $payload['status']);
        }
    }
}
