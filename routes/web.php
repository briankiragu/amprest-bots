<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// Routes for the Telegram Bot.
$router->group(['prefix' => 'telegram'], function () use ($router) {
    $router->post('/{token}/new-message', [
        'as' => 'profile', 'uses' => 'TelegramController@newMessage'
    ]);
});
